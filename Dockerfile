FROM nginx:latest

ENV SERVICE_NAME=your-service-name

LABEL "com.datadoghq.ad.logs"='[{"source": "berserkers", "service": "${SERVICE_NAME}"}]'