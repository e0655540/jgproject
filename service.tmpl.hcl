job "${SERVICE_NAME}" {
  datacenters = ["rsc-cni-nonprod"]
  type = "service"
  update {
    stagger = "10s"
    max_parallel = 1
  }

  group "${SERVICE_NAME}" {
    count = 1

    task "${SERVICE_NAME}" {
      driver = "docker"
      config {
        image = "${DOCKER_IMAGE}"
        port_map {
          http = "${PORT}"
        }
        logging {
          type = "journald"
          config {
            tag = "${SERVICE_NAME}"
          }
        }
      }

      service {
        name = "${SERVICE_NAME}"
        port = "http"
        tags = [
          "traefik.tags=public"
        ]

        check {
          type = "http"
          path = "/"
          interval = "10s"
          timeout = "5s"
        }
      }
      
      env {
        "SERVICE_NAME" = "${SERVICE_NAME}"
      }

      resources {
        cpu = 100
        memory = 128

        network {
          mbits = 10
          port "http" {}
        }
      }
    }
  }
}
